package com.phildatoon.impl;

import com.phildatoon.constants.CommonConstants;
import com.phildatoon.inf.PricingRule;
import com.phildatoon.inf.Product;
import com.phildatoon.pricingrules.PricingRule2016;
import com.phildatoon.servicefactory.ProductFactory;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.*;

/**
 * Initial version by PhilDatoon on 01 Dec 2016
 */
public class ShoppingCartTest {
   private final ProductFactory pf = new ProductFactory();
   private final PricingRule rule = new PricingRule2016();

   private Product prodSmall = pf.getProduct(CommonConstants.PROD_CODE_ULT_SMALL);
   private Product prodMedium = pf.getProduct(CommonConstants.PROD_CODE_ULT_MEDIUM);
   private Product prodLarge = pf.getProduct(CommonConstants.PROD_CODE_ULT_LARGE);
   private Product prodData = pf.getProduct(CommonConstants.PROD_CODE_DATA_1GB);

   @Test
   public void testScenario1() {
      ShoppingCart myCart = new ShoppingCart(rule);
      myCart.add(prodSmall);
      myCart.add(prodSmall);
      myCart.add(prodSmall);
      myCart.add(prodLarge);

      myCart.total();
      myCart.items();

      Map<String, Integer> itemList = myCart.getItemQty();

      assertNotNull(myCart.getRule());
      assertNull(myCart.getPromoCode());

      assertEquals(94.7, myCart.getTotal(), 1);

      assertEquals(3, getQtyByProduct(itemList, CommonConstants.PROD_CODE_ULT_SMALL));
      assertEquals(0, getQtyByProduct(itemList, CommonConstants.PROD_CODE_ULT_MEDIUM));
      assertEquals(1, getQtyByProduct(itemList, CommonConstants.PROD_CODE_ULT_LARGE));
      assertEquals(0, getQtyByProduct(itemList, CommonConstants.PROD_CODE_DATA_1GB));
   }

   @Test
   public void testScenario2() {
      ShoppingCart myCart = new ShoppingCart(rule);
      myCart.add(prodSmall);
      myCart.add(prodSmall);
      myCart.add(prodLarge);
      myCart.add(prodLarge);
      myCart.add(prodLarge);
      myCart.add(prodLarge);

      myCart.total();
      myCart.items();

      Map<String, Integer> itemList = myCart.getItemQty();

      assertNotNull(myCart.getRule());
      assertNull(myCart.getPromoCode());

      assertEquals(209.4, myCart.getTotal(), 1);

      assertEquals(2, getQtyByProduct(itemList, CommonConstants.PROD_CODE_ULT_SMALL));
      assertEquals(0, getQtyByProduct(itemList, CommonConstants.PROD_CODE_ULT_MEDIUM));
      assertEquals(4, getQtyByProduct(itemList, CommonConstants.PROD_CODE_ULT_LARGE));
      assertEquals(0, getQtyByProduct(itemList, CommonConstants.PROD_CODE_DATA_1GB));
   }

   @Test
   public void testScenario3() {
      ShoppingCart myCart = new ShoppingCart(rule);
      myCart.add(prodSmall);
      myCart.add(prodMedium);
      myCart.add(prodMedium);

      myCart.total();
      myCart.items();

      Map<String, Integer> itemList = myCart.getItemQty();

      assertNotNull(myCart.getRule());
      assertNull(myCart.getPromoCode());

      assertEquals(84.7, myCart.getTotal(), 1);

      assertEquals(1, getQtyByProduct(itemList, CommonConstants.PROD_CODE_ULT_SMALL));
      assertEquals(2, getQtyByProduct(itemList, CommonConstants.PROD_CODE_ULT_MEDIUM));
      assertEquals(0, getQtyByProduct(itemList, CommonConstants.PROD_CODE_ULT_LARGE));
      assertEquals(2, getQtyByProduct(itemList, CommonConstants.PROD_CODE_DATA_1GB));
   }

   @Test
   public void testScenario4() {
      ShoppingCart myCart = new ShoppingCart(rule);
      myCart.add(prodSmall);
      myCart.add(prodData, "I<3AMAYSIM");

      myCart.total();
      myCart.items();

      Map<String, Integer> itemList = myCart.getItemQty();

      assertNotNull(myCart.getRule());
      assertEquals("I<3AMAYSIM", myCart.getPromoCode());

      assertEquals(31.32, myCart.getTotal(), 1);

      assertEquals(1, getQtyByProduct(itemList, CommonConstants.PROD_CODE_ULT_SMALL));
      assertEquals(0, getQtyByProduct(itemList, CommonConstants.PROD_CODE_ULT_MEDIUM));
      assertEquals(0, getQtyByProduct(itemList, CommonConstants.PROD_CODE_ULT_LARGE));
      assertEquals(1, getQtyByProduct(itemList, CommonConstants.PROD_CODE_DATA_1GB));
   }

   private int getQtyByProduct(Map<String, Integer> itemList, String prodCode) {
      if (itemList.containsKey(prodCode)) {
         return itemList.get(prodCode);
      } else {
         return 0;
      }
   }
}
