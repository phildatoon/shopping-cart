package com.phildatoon.main;

import com.phildatoon.impl.ShoppingCart;
import com.phildatoon.inf.PricingRule;
import com.phildatoon.inf.Product;
import com.phildatoon.pricingrules.PricingRule2016;
import com.phildatoon.servicefactory.ProductFactory;

/**
 * Initial version by PhilDatoon on 23 Nov 2016
 */
public class ClientShoppingCart {
   public static void main(String[] args) {
      ProductFactory pf = new ProductFactory();
      Product prodSmall = pf.getProduct("ult_small");
      Product prodMedium = pf.getProduct("ult_medium");
      Product prodLarge = pf.getProduct("ult_large");
      Product prodData = pf.getProduct("1gb");

      PricingRule rule = new PricingRule2016();

      // Case #1
      System.out.println("Scenario #1");

      ShoppingCart myCart1 = new ShoppingCart(rule);
      myCart1.add(prodSmall);
      myCart1.add(prodSmall);
      myCart1.add(prodSmall);
      myCart1.add(prodLarge);

      myCart1.total();
      myCart1.items();

      // Case #2
      System.out.println();
      System.out.println("Scenario #2");

      ShoppingCart myCart2 = new ShoppingCart(rule);
      myCart2.add(prodSmall);
      myCart2.add(prodSmall);
      myCart2.add(prodLarge);
      myCart2.add(prodLarge);
      myCart2.add(prodLarge);
      myCart2.add(prodLarge);

      myCart2.total();
      myCart2.items();

      // Case #3
      System.out.println();
      System.out.println("Scenario #3");

      ShoppingCart myCart3 = new ShoppingCart(rule);
      myCart3.add(prodSmall);
      myCart3.add(prodMedium);
      myCart3.add(prodMedium);

      myCart3.total();
      myCart3.items();

      // Case #4
      System.out.println();
      System.out.println("Scenario #4");

      ShoppingCart myCart4 = new ShoppingCart(rule);
      myCart4.add(prodSmall);
      myCart4.add(prodData, "I<3AMAYSIM");

      myCart4.total();
      myCart4.items();

      System.exit(0);
   }
}