package com.phildatoon.pricingrules;

import com.phildatoon.constants.CommonConstants;
import com.phildatoon.inf.PricingRule;
import com.phildatoon.inf.Product;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Initial version by PhilDatoon on 23 Nov 2016
 */
public class PricingRule2016 extends PricingRule {
   private Product prodSmall = pf.getProduct(CommonConstants.PROD_CODE_ULT_SMALL);
   private Product prodMedium = pf.getProduct(CommonConstants.PROD_CODE_ULT_MEDIUM);
   private Product prodLarge = pf.getProduct(CommonConstants.PROD_CODE_ULT_LARGE);
   private Product prodData = pf.getProduct(CommonConstants.PROD_CODE_DATA_1GB);

   @Override
   public List<Product> getNondiscountedItems() {
      return Arrays.asList(prodData);
   }

   @Override
   public double applyPrimaryRule(Map<String, Integer> itemList) {
      double promoTotalAmt = 0;

      promoTotalAmt += promoUltSmall(itemList, prodSmall);
      promoTotalAmt += promoUltLarge(itemList, prodLarge);
      promoTotalAmt += promoUltMedium(itemList, prodMedium);

      return promoTotalAmt;
   }

   @Override
   public double applySecondaryRule(Map<String, Integer> itemList) {
      return 0;
   }

   @Override
   public double applyPromoCode(Map<String, Integer> itemList, String code, double total) {
      if (null == code) {
         return total;
      }

      switch (code) {
         case CommonConstants.PROMO_CODE:
            return total * (1 - CommonConstants.PROMO_DISCOUNT_10);
         default:
            return total;
      }
   }

   private double promoUltSmall(Map<String, Integer> itemList, Product prodSmall) {
      int prodQty = getQtyByProduct(itemList, prodSmall.getProductCode());
      double discountedAmt = 0;

      if (prodQty >= CommonConstants.MAX_QTY_PER_DEAL_ULT_SMALL) {
         discountedAmt = prodSmall.getPrice() * CommonConstants.QTY_PROMO_ULT_SMALL *
                         (prodQty / CommonConstants.MAX_QTY_PER_DEAL_ULT_SMALL);
      }

      return discountedAmt + ((prodQty % CommonConstants.MAX_QTY_PER_DEAL_ULT_SMALL) * prodSmall.getPrice());
   }

   private double promoUltLarge(Map<String, Integer> itemList, Product prodLarge) {
      int prodQty = getQtyByProduct(itemList, prodLarge.getProductCode());

      if (prodQty > CommonConstants.MIN_QTY_BULK_ULT_LARGE) {
         return prodQty * CommonConstants.BULK_ULT_LARGE_PRICE;
      } else {
         return prodQty * prodLarge.getPrice();
      }
   }

   private double promoUltMedium(Map<String, Integer> itemList, Product prodMedium) {
      int prodQty = getQtyByProduct(itemList, prodMedium.getProductCode());
      updateQtyByProduct(itemList, CommonConstants.PROD_CODE_DATA_1GB, prodQty);
      return prodQty * prodMedium.getPrice();
   }
}