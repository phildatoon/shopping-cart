package com.phildatoon.products;

import com.phildatoon.constants.CommonConstants;
import com.phildatoon.inf.Product;

/**
 * Initial version by PhilDatoon on 23 Nov 2016
 */
public class Data1Gb extends Product {
   public Data1Gb() {
      setProductCode(CommonConstants.PROD_CODE_DATA_1GB);
      setProductName("1GB Data-pack");
      setPrice(9.90);
   }
}