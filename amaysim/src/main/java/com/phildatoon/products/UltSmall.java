package com.phildatoon.products;

import com.phildatoon.constants.CommonConstants;
import com.phildatoon.inf.Product;

/**
 * Initial version by PhilDatoon on 23 Nov 2016
 */
public class UltSmall extends Product {
   public UltSmall() {
      setProductCode(CommonConstants.PROD_CODE_ULT_SMALL);
      setProductName("Unlimited 1GB");
      setPrice(24.90);
   }
}
