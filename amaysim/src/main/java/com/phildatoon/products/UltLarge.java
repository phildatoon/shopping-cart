package com.phildatoon.products;

import com.phildatoon.constants.CommonConstants;
import com.phildatoon.inf.Product;

/**
 * Initial version by PhilDatoon on 23 Nov 2016
 */
public class UltLarge extends Product {
   public UltLarge() {
      setProductCode(CommonConstants.PROD_CODE_ULT_LARGE);
      setProductName("Unlimited 5GB");
      setPrice(44.90);
   }
}