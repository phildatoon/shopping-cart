package com.phildatoon.products;

import com.phildatoon.constants.CommonConstants;
import com.phildatoon.inf.Product;

/**
 * Initial version by PhilDatoon on 23 Nov 2016
 */
public class UltMedium extends Product {
   public UltMedium() {
      setProductCode(CommonConstants.PROD_CODE_ULT_MEDIUM);
      setProductName("Unlimited 2GB");
      setPrice(29.90);
   }
}
