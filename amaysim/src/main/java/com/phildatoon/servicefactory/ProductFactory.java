package com.phildatoon.servicefactory;

import com.phildatoon.inf.Product;
import com.phildatoon.products.Data1Gb;
import com.phildatoon.products.UltLarge;
import com.phildatoon.products.UltMedium;
import com.phildatoon.products.UltSmall;

import java.util.HashMap;
import java.util.Map;

/**
 * Initial version by PhilDatoon on 23 Nov 2016
 */
public class ProductFactory {
   public Product getProduct(String productCode) {
      Map<String, Product> map = new HashMap<>();
      map.put("ult_small", new UltSmall());
      map.put("ult_medium", new UltMedium());
      map.put("ult_large", new UltLarge());
      map.put("1gb", new Data1Gb());

      return map.get(productCode);
   }
}
