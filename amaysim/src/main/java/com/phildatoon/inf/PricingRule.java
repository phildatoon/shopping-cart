package com.phildatoon.inf;

import com.phildatoon.servicefactory.ProductFactory;

import java.util.List;
import java.util.Map;

/**
 * Initial version by PhilDatoon on 23 Nov 2016
 */
public abstract class PricingRule {
   protected final ProductFactory pf = new ProductFactory();

   public final double applyPricingRule(Map<String, Integer> itemList, String code) {
      double total = 0;

      total += nondiscountedItems(itemList, getNondiscountedItems());
      total += applyPrimaryRule(itemList);
      total += applySecondaryRule(itemList);
      total = applyPromoCode(itemList, code, total);

      return total;
   }

   protected abstract List<Product> getNondiscountedItems();

   protected abstract double applyPrimaryRule(Map<String, Integer> itemList);

   protected abstract double applySecondaryRule(Map<String, Integer> itemList);

   protected abstract double applyPromoCode(Map<String, Integer> itemList, String code, double total);

   protected final double nondiscountedItems(Map<String, Integer> itemList, List<Product> productList) {
      double nondiscountedTotal = 0;

      for (Product product : productList) {
         int prodQty = getQtyByProduct(itemList, product.getProductCode());
         nondiscountedTotal += prodQty * product.getPrice();
      }

      return nondiscountedTotal;
   }

   protected final Integer getQtyByProduct(Map<String, Integer> itemList, String product) {
      if (itemList.containsKey(product)) {
         return itemList.get(product);
      } else {
         return 0;
      }
   }

   protected final Integer updateQtyByProduct(Map<String, Integer> itemList, String product, Integer newValue) {
      if (itemList.containsKey(product)) {
         return itemList.put(product, itemList.get(product) + newValue);
      } else {
         return itemList.put(product, newValue);
      }
   }
}