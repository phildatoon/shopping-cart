package com.phildatoon.inf;

/**
 * Initial version by PhilDatoon on 23 Nov 2016
 */
public interface Cart {

   void add(Product item1);

   void add(Product item1, String promoCode);

   void total();

   void items();
}
