package com.phildatoon.impl;

import com.phildatoon.constants.CommonConstants;
import com.phildatoon.inf.Cart;
import com.phildatoon.inf.PricingRule;
import com.phildatoon.inf.Product;
import com.phildatoon.servicefactory.ProductFactory;

import java.util.*;

/**
 * Initial version by PhilDatoon on 23 Nov 2016
 */
public class ShoppingCart implements Cart {
   private PricingRule rule;
   private String promoCode;
   private double total;

   private List<Product> itemList = new ArrayList<>();
   private Map<String, Integer> itemQty = new HashMap<>();

   private ProductFactory pf = new ProductFactory();

   public ShoppingCart(PricingRule rule) {
      this.rule = rule;
   }

   @Override
   public void add(Product item) {
      try {
         itemList.add(item);
         updateItemCount(item);
      } catch (Exception e) {
         System.err.println(e.getClass().getCanonicalName() + " found: " + e.getMessage());
      }
   }

   @Override
   public void add(Product item, String promoCode) {
      add(item);
      this.promoCode = promoCode;
   }

   @Override
   public void total() {
      try {
         total = rule.applyPricingRule(itemQty, promoCode);
         System.out.println(String.format("Total: $ %.2f", total));
      } catch (Exception e) {
         System.err.println(e.getClass().getCanonicalName() + " found: " + e.getMessage());
      }
   }

   @Override
   public void items() {
      try {
         Iterator entries = itemQty.entrySet().iterator();
         while (entries.hasNext()) {
            Map.Entry entry = (Map.Entry) entries.next();
            String key = (String) entry.getKey();
            String prodName = pf.getProduct(key).getProductName();
            int qty = (Integer) entry.getValue();

            if (0 == qty) {
               continue;
            }

            System.out.println(String.format("%3d x %s", qty, prodName));
         }
      } catch (Exception e) {
         System.err.println(e.getClass().getCanonicalName() + " found: " + e.getMessage());
      }
   }

   private void updateItemCount(Product item) {
      try {
         String code = item.getProductCode();

         if (itemQty.containsKey(code)) {
            itemQty.put(code, itemQty.get(code) + CommonConstants.ITERATOR);
         } else {
            itemQty.put(code, CommonConstants.ITERATOR);
         }
      } catch (Exception e) {
         System.err.println(e.getClass().getCanonicalName() + " found: " + e.getMessage());
      }
   }

   public PricingRule getRule() {
      return rule;
   }

   public String getPromoCode() {
      return promoCode;
   }

   public double getTotal() {
      return total;
   }

   public List<Product> getItemList() {
      return itemList;
   }

   public Map<String, Integer> getItemQty() {
      return itemQty;
   }

   public ProductFactory getPf() {
      return pf;
   }

}