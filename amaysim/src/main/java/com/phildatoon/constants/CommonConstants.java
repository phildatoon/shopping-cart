package com.phildatoon.constants;

/**
 * Initial version by PhilDatoon on 23 Nov 2016
 */
public final class CommonConstants {
   private CommonConstants() {
   }

   public static final String PROMO_CODE = "I<3AMAYSIM";
   public static final String PROD_CODE_ULT_SMALL = "ult_small";
   public static final String PROD_CODE_ULT_MEDIUM = "ult_medium";
   public static final String PROD_CODE_ULT_LARGE = "ult_large";
   public static final String PROD_CODE_DATA_1GB = "1gb";

   public static final double PROMO_DISCOUNT_10 = 0.10;
   public static final double BULK_ULT_LARGE_PRICE = 39.90;

   public static final int ITERATOR = 1;
   public static final int QTY_PROMO_ULT_SMALL = 2;
   public static final int MAX_QTY_PER_DEAL_ULT_SMALL = 3;
   public static final int MIN_QTY_BULK_ULT_LARGE = 3;

}